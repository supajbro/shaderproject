﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObj : MonoBehaviour
{

    public float z = 0.5f;

    void Update()
    {
        transform.Rotate(new Vector3(0, z, 0)); //applying rotation.
    }

}
